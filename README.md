# QuduratPSDM

## Overview
QuduratPSDM is a web-based platform designed to simulate the General Aptitude Test for University Graduates in Saudi Arabia. The platform features a variety of test simulations that cover different difficulty levels and aptitude types. The primary goal of QuduratPSDM is to provide users with a realistic testing environment to practice and prepare for actual aptitude exams.

## Exam Structure
The exam on QuduratPSDM consists of 6 parts, with each part containing around 16-24 questions. Each part has a time limit of approximately 20-30 minutes, with a countdown timer displayed to keep track of the remaining time. The parts are sequential, meaning that when a user opens the website, they will find the first part, and after the time is up, they will be automatically redirected to the next part. This process continues until the last part of the exam.

## Getting Started
To start using QuduratPSDM, simply navigate to the website and begin with the first part of the exam. As you complete each part, you will be automatically redirected to the next one. Good luck with your preparation!

## Contact
For any further questions or feedback, please feel free to reach out to us.

## License
This project is licensed under the terms of the MIT license.